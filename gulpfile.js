const {src, dest, series, parallel} = require('gulp');
const htmlmin = require('gulp-htmlmin');
const cleanCSS = require('gulp-clean-css');
const concatCSS = require('gulp-concat-css');
const browserSync = require('browser-sync');

const browser = browserSync.create()

//first html task
function html(){
    return src('src/index.html')
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(dest('build/'))
}

function css(){
    return src('src/css/*.css')
        .pipe(concatCSS('style.css'))
        .pipe(cleanCSS())
        .pipe(dest('build/css/'))
}

function live(){
    browser.init({
        server: {
            baseDir: 'build/'
        }
    })
}

exports.html = html;
exports.css = css;
exports.live = live;

exports.build = parallel(html, css);